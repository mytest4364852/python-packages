import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="baitong", # Replace with your own username
    version="1.0.0",   
    author="Tanaporn Songprayad",
    author_email="wasinlaspy@gmail.com",
    description="A package to processing Vallaris Maps",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://v2k.vallarismaps.com",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires= ['jsonschema','ndjson','geojson','requests','geopandas','pandas', 'rtree', 'numpy','shapely','tqdm', 'python-dotenv', 'matplotlib', 'earthpy', 'uuid'],
    python_requires='>=3.6',
)

# pip3 install valBB @git+https://github.com/sattawatarab/py_lab

# pip install "Package" @ git+"URL of the repository"

# pip3 install py_lab@git+https://github.com/sattawatarab/py_lab

# https://github.com/sattawatarab/py_lab


